  /*fact_funnel_accumulating is a query that creates one row per quote per day until binded
and uses the d2a.accounts table*/
WITH
  base_table AS (
  SELECT
    policyID,
    insuredID,
    email,
    status,
    policyNum,
    CAST(insuredAddDate AS DATE) AS insuredAddDate,
    CAST(effectiveDate AS DATE) AS effectiveDate,
    CAST(applicationDate AS DATE) AS applicationDate,
    CAST(cancelledDate AS DATE) AS canceledDate,
    insuredAddDate AS insuranceAddDateTime,
    effectiveDate AS effectiveDateTime,
    applicationDate AS applicationDateTime,
    cancelledDate AS canceledDateTime,
    ROW_NUMBER() OVER (PARTITION BY email ORDER BY insuredAddDate ASC) quote_num
  FROM
    `datalakewarehouse.d2a_datasets.accounts`
  WHERE
    status != 'RC1A'),
  add_quote_bind_flag AS(
  SELECT
    policyID,
    email,
    status,
    policyNum,
    insuredID,
    CASE
      WHEN status = "RC1A" THEN 0
      WHEN status = "RC1B" THEN 1
      WHEN status = "RC2" THEN 1
      WHEN status = "Not_Written" THEN 1
      WHEN status = "Active" THEN 1
      WHEN status = "Canceled" THEN 1
      WHEN status = "Expired" THEN 1
      WHEN status = "Bound" THEN 1
    ELSE
    0
  END
    quoted,
    CASE
      WHEN status = "RC1A" THEN 0
      WHEN status = "RC1B" THEN 0
      WHEN status = "RC2" THEN 0
      WHEN status = "Not_Written" THEN 0
      WHEN status = "Active" THEN 1
      WHEN status = "Canceled" THEN 1
      WHEN status = "Expired" THEN 1
      WHEN status = "Bound" THEN 1
    ELSE
    0
  END
    bind_flag,
    insuredAddDate,
    applicationDate,
    effectiveDate,
    insuranceAddDateTime,
    effectiveDateTime,
    applicationDateTime,
    canceledDateTime,
    canceledDate,
    quote_num
  FROM
    base_table ),
  all_first_quotes AS (
  SELECT
    policyID,
    insuredID,
    email,
    status,
    insuranceAddDateTime AS quote_date_time,
    insuredAddDate AS quote_date,
    quoted,
    bind_flag
  FROM
    add_quote_bind_flag
  WHERE
    quote_num = 1
    AND quoted = 1),
  all_binds AS (
  SELECT
    email,
    status,
    applicationDate AS bind_date,
    applicationDateTime AS bind_date_time,
    canceledDate AS canceled_date,
    bind_flag
  FROM
    add_quote_bind_flag
  WHERE
    bind_flag = 1 ),
  all_binds_ranked AS (
  SELECT
    email,
    status,
    bind_date,
    bind_date_time,
    canceled_date,
    bind_flag,
    ROW_NUMBER() OVER (PARTITION BY email ORDER BY bind_date_time ASC) AS bind_rank
  FROM
    all_binds ),
  first_bind AS (
  SELECT
    email,
    status,
    bind_date,
    bind_date_time,
    bind_flag,
    canceled_date
  FROM
    all_binds_ranked
  WHERE
    bind_rank = 1 )
  #snapshot one row per email joined with first quote and first bind
  ,
  snapshot_first_quote_bind AS (
  SELECT
    all_first_quotes.email,
    all_first_quotes.quote_date,
    all_first_quotes.quote_date_time,
    all_first_quotes.quoted,
    bind_date,
    bind_date_time,
    canceled_date,
    COALESCE(first_bind.bind_flag,
      0) AS bind_flag
  FROM
    all_first_quotes
  LEFT JOIN
    first_bind
  ON
    first_bind.email = all_first_quotes.email),
  snapshot_binds AS (
  SELECT
    email,
    quote_date,
    quote_date_time,
    quoted,
    bind_date,
    bind_date_time,
    bind_flag,
    canceled_date
  FROM
    snapshot_first_quote_bind
  WHERE
    bind_flag = 1 ),
  accumulating_binds AS (
  SELECT
    email,
    dim.as_of_date_k,
    dim.full_date,
    quote_date,
    quote_date_time,
    bind_date,
    bind_date_time,
    quoted,
    canceled_date,
    CASE
      WHEN DATE_DIFF(dim.full_date, bind_date, DAY) = 0 THEN 1
    ELSE
    0
  END
    AS binded
  FROM
    `datalakewarehouse.d2a_datasets.dim_date` AS dim
  LEFT JOIN
    snapshot_binds
  ON
    snapshot_binds.quote_date <= dim.full_date
    AND CURRENT_DATE() >= dim.full_date
  WHERE
    email IS NOT NULL ),
  accumulating_quotes AS (
  SELECT
    email,
    dim.as_of_date_k,
    dim.full_date,
    quote_date_time,
    quote_date,
    bind_date,
    bind_date_time,
    quoted,
    canceled_date,
    0 AS binded
  FROM
    `datalakewarehouse.d2a_datasets.dim_date` AS dim
  LEFT JOIN
    snapshot_first_quote_bind
  ON
    snapshot_first_quote_bind.quote_date <= dim.full_date
    AND CURRENT_DATE() >= dim.full_date
  WHERE
    email IS NOT NULL
    AND bind_flag = 0 ),
  union_transactions AS ( (
    SELECT
      email,
      as_of_date_k,
      full_date,
      quote_date_time,
      bind_date_time,
      quoted,
      binded,
      quote_date,
      bind_date,
      canceled_date
    FROM
      accumulating_binds)
  UNION ALL (
    SELECT
      email,
      as_of_date_k,
      full_date,
      quote_date_time,
      bind_date_time,
      quoted,
      binded,
      quote_date,
      bind_date,
      canceled_date
    FROM
      accumulating_quotes) )
SELECT
  full_date AS as_of_date,
  as_of_date_k,
  email,
  quote_date_time,
  bind_date_time,
  CASE
    WHEN DATE_DIFF(quote_date, full_date, DAY) = 0 THEN 1
  ELSE
  0
END
  AS quote_on_day,
  quoted AS quote_by_day,
  DATE_DIFF(full_date,quote_date, DAY) AS quote_development_days,
  binded AS bind_on_day,
  CASE
    WHEN DATE_DIFF(full_date,bind_date, DAY) >= 0 THEN 1
  ELSE
  0
END
  AS bind_by_day,
  CASE
    WHEN DATE_DIFF( full_date, bind_date,DAY) < 0 THEN 0
  ELSE
  COALESCE(DATE_DIFF(full_date,bind_date,DAY),
    0)
END
  AS bind_development_days,
  CASE
    WHEN DATE_DIFF(canceled_date,full_date, DAY) = 0 THEN 1
  ELSE
  0
END
  AS cancel_on_day,
  CASE
    WHEN DATE_DIFF( full_date,canceled_date, DAY) >= 0 THEN 1
  ELSE
  0
END
  AS cancel_by_day,
  CASE
    WHEN DATE_DIFF( full_date,canceled_date, DAY) < 0 THEN 0
  ELSE
  COALESCE(DATE_DIFF(full_date,canceled_date, DAY),
    0)
END
  AS cancel_development_days
FROM
  union_transactions;