DECLARE LastMonthStart STRING ;
DECLARE LastMonthEnd STRING ;
SET LastMonthStart = cast(FORMAT_DATE("%m/%d/%Y",DATE_SUB(DATE_TRUNC(CURRENT_DATE(), MONTH), INTERVAL 1 MONTH)) as STRING) ;
SET LastMonthEnd = cast(FORMAT_DATE("%m/%d/%Y",DATE_SUB(DATE_TRUNC(CURRENT_DATE(), MONTH), INTERVAL 1 DAY)) as STRING) ;

WITH Policy_Select AS(
        SELECT policyID, policyNum, status,
            FORMAT_DATE("%m/%d/%Y", PARSE_TIMESTAMP('%Y-%m-%dT%H:%M:%E*SZ',CAST(effectiveDate AS string))) AS Policy_Eff_Date,
            FORMAT_DATE("%m/%d/%Y", PARSE_TIMESTAMP('%Y-%m-%dT%H:%M:%E*SZ',CAST(expirationDate AS string))) AS Policy_Exp_Date,
            paymentPlanID
        FROM `roadrisk-244718.silvervine.Policy`
        WHERE policyNum like 'PPA%'),

    PaymentPlan_Select AS(
        SELECT paymentPlanID, planName
        FROM `roadrisk-244718.silvervine.PaymentPlan`),

    Zipcode AS(
        SELECT zip.policyID, zip.defaultGaragingAddressID, zipcode
        FROM `roadrisk-244718.silvervine.Auto` zip
        JOIN
            (SELECT ad.id, ad.zipcode FROM `roadrisk-244718.silvervine.Address` ad) ad_select
            ON zip.defaultGaragingAddressID = ad_select.id),

    Driver_Name AS(
        SELECT policyID, fname, lname
        FROM `roadrisk-244718.silvervine.Driver`
        WHERE driverNumber = 1),

    CoveragePremium_Select AS(
        SELECT *,
            ROW_NUMBER() OVER(PARTITION BY policyID ORDER BY Trans_Date, Trans_Eff_Date, changeType,
            CASE WHEN coverage = 'BI' THEN '1'WHEN coverage = 'PD' THEN '2' ELSE coverage END) AS rank_coverage
        FROM
            (SELECT policyID, FORMAT_DATE("%m/%d/%Y", PARSE_TIMESTAMP('%Y-%m-%dT%H:%M:%E*SZ',transactionDate)) AS Trans_Date,
            FORMAT_DATE("%m/%d/%Y", PARSE_TIMESTAMP('%Y-%m-%dT%H:%M:%E*SZ',dateEffective)) AS Trans_Eff_Date, changeType,
            CASE changeType
                WHEN 0 THEN 'Preactive'
                WHEN 1 THEN 'New Policy'
                WHEN 2 THEN 'Endorsement'
                WHEN 3 THEN 'Cancellation Eff'
                WHEN 4 THEN 'Other'
                WHEN 5 THEN 'Flat Cancellation'
                WHEN 6 THEN 'Reinstatement'
                END AS Change_Type_Name1,
            coverage, changeInTPD
            FROM `roadrisk-244718.silvervine.CoveragePremium` a
            ORDER BY policyID, Trans_Date, Trans_Eff_Date)
        WHERE Trans_Date BETWEEN LastMonthStart AND LastMonthEnd
        ORDER BY policyID, Trans_Date, Trans_Eff_Date),

    CoveragePremium_limit AS(
        SELECT * EXCEPT (policyID2, coverage2, prev_premium, ChangeDate)
        FROM CoveragePremium_Select a
        LEFT JOIN
            (SELECT distinct policyID AS policyID2, coverage AS coverage2,
            CASE limit1
                WHEN 0 THEN ''
                ELSE CONCAT(CAST(limit1 AS STRING),'')
                END AS limit1_v1,
            CASE limit2
                WHEN 0 THEN ''
                ELSE CONCAT('/', CAST(limit2 AS STRING))
                END AS limit2_v1,
            CASE limit3
                WHEN 0 THEN ''
                ELSE CONCAT( '/',CAST(limit3 AS STRING))
                END AS limit3_v1,
            CASE deductible
                WHEN 0 THEN ''
                ELSE CONCAT(CAST(deductible AS STRING), '')
                END AS deductible_v1,
            premium-premiumChange AS prev_premium,
            FORMAT_DATE("%m/%d/%Y", PARSE_TIMESTAMP('%Y-%m-%dT%H:%M:%E*SZ',addDate)) AS ChangeDate,
            FROM `roadrisk-244718.silvervine.Coverages`) b
        ON a.policyID = b.policyID2 AND a.coverage = b.coverage2
        AND a.Trans_Date = b.ChangeDate
        AND CAST(a.changeInTPD AS int) = CAST(b.prev_premium AS int)
        ORDER BY policyID, ChangeDate),

    Vehicle_Num AS(
        SELECT policyID, count(vehicleID) AS Num_Vehicles, 1 AS rank_1
        FROM `roadrisk-244718.silvervine.Vehicle`
        GROUP BY policyID),

    MVCPA_Fee AS(
        SELECT month_year, policyID, sum(State_Fee_or_Tax) AS State_Fee_or_Tax, State_Fee_or_Tax_Description
        FROM
            (SELECT FORMAT_DATE("%Y-%m", PARSE_TIMESTAMP('%Y-%m-%dT%H:%M:%E*SZ',ar.arDate)) AS month_year,
                ar.policyID, ar.amount AS State_Fee_or_Tax,
                CASE
                    WHEN ar.arTypeID = 22 THEN 'MVCPA Fee'
                    END AS State_Fee_or_Tax_Description
            FROM `roadrisk-244718.silvervine.AR` ar
            JOIN `roadrisk-244718.silvervine.ARType` art ON ar.arTypeID = art.arTypeID
            WHERE ar.arTypeID = 22) a
        GROUP BY month_year, policyID, State_Fee_or_Tax_Description
        ORDER BY policyID),

    Agency_Fee AS(
        SELECT month_year, policyID, sum(Agency_Fee) AS Agency_Fee
        FROM
            (SELECT FORMAT_DATE("%Y-%m", PARSE_TIMESTAMP('%Y-%m-%dT%H:%M:%E*SZ',ar.arDate)) AS month_year,
                ar.policyID AS policyID, ar.amount AS Agency_Fee
            FROM `roadrisk-244718.silvervine.AR` ar
            JOIN `roadrisk-244718.silvervine.ARType` art ON ar.arTypeID = art.arTypeID
            WHERE ar.arTypeID = 7)
        GROUP BY month_year, policyID
        ORDER BY policyID),

    Renewal_Int AS(
        SELECT Pol.insuredID, policyNum, policyID, status,isRenewal,
            ROW_NUMBER() OVER(PARTITION BY Pol.insuredID ORDER BY effectiveDate DESC) AS policynum_order
        FROM `roadrisk-244718.silvervine.Policy` Pol
        INNER JOIN
            (SELECT distinct Ins.insuredID
            FROM `roadrisk-244718.silvervine.Policy` Ins
            WHERE Ins.policyNum like 'PPA%'
            GROUP BY  Ins.insuredID
            HAVING COUNT(Ins.insuredID) > 1) MI
        ON Pol.insuredID = MI.insuredID),

    Renewal_Fin AS (
        SELECT R1.insuredID,
            R1.policyNum AS CurrentPolicyNum,
            R2.policyNum AS Expiring_Policy_Num,
            R1.policyID AS CurrentPolicyID,
            R2.policyID AS Expiring_Policy_ID,
            R1.status AS CurrentPolicyStatus,
            R2.status AS PreviousPolicyStatus,
            R1.isRenewal AS Renewal
        FROM Renewal_Int R1
        JOIN Renewal_Int R2 ON R1.insuredID = R2.insuredID AND R1.policynum_order = 1 AND R2.policynum_order = 2
        WHERE (R1.status <> 0 or R1.status <> 6) and R1.isRenewal = 1
        ORDER BY R1.insuredID ),

    half_completed_table AS(
        SELECT FORMAT_DATE("%Y%m", PARSE_TIMESTAMP('%m/%d/%Y',Trans_Date)) AS Bordereau_Month,
            'LOO' AS Agency, 'OIC' AS Writing_Company,
            concat(lname, ', ', fname) AS Insured_Name, zipcode AS Insured_Zip,
            a.policyID, policyNum AS Policy_Num,
            Policy_Eff_Date, Policy_Exp_Date, Trans_Date, Trans_Eff_Date,

            CASE
                WHEN Change_Type_Name1 = 'New Policy'and Renewal = 1 THEN 'Renewal'
                ELSE Change_Type_Name1 END AS Change_Type_Name ,
            replace(coverage, 'OTC', 'COMP') AS Coverage,
            CASE
                WHEN Change_Type_Name1 = 'Cancellation Eff' or Change_Type_Name1 = 'Flat Cancellation' THEN 'Cancel'
                ELSE concat(limit1_v1, limit2_v1, limit3_v1)
                END AS limits,
            deductible_v1 AS deductible,
            Num_Vehicles, a.status,
            changeInTPD AS Written_Premium,
            planName AS Payment_Plan,
            CASE
                WHEN State_Fee_or_Tax is null THEN 0
                ELSE State_Fee_or_Tax
                END AS State_Fee_or_Tax,
            CASE
                WHEN State_Fee_or_Tax is null THEN '-'
                ELSE State_Fee_or_Tax_Description
                END AS State_Fee_or_Tax_Description,
            Agency_Fee,
            0 AS Carrier_Fees,
            Expiring_Policy_Num
        FROM Policy_Select a
        LEFT JOIN PaymentPlan_Select pp on a.paymentPlanID = pp.paymentPlanID
        LEFT JOIN Zipcode z on a.policyID = z.policyID
        LEFT JOIN CoveragePremium_limit c on a.policyID = c.policyID
        LEFT JOIN Vehicle_Num v on v.policyID = a.policyID AND c.rank_coverage = v.rank_1
        LEFT JOIN Driver_Name d on a.policyID = d.policyID
        LEFT JOIN MVCPA_Fee mf on a.policyID = mf.policyID AND mf.month_year = FORMAT_DATE("%Y-%m", PARSE_TIMESTAMP('%m/%d/%Y',Trans_Date)) AND c.rank_coverage = 1
        LEFT JOIN Agency_Fee af on a.policyID = af.policyID AND af.month_year = FORMAT_DATE("%Y-%m", PARSE_TIMESTAMP('%m/%d/%Y',Trans_Date)) AND c.rank_coverage = 1
        LEFT JOIN Renewal_Fin r on a.policyID = r.CurrentPolicyID
)

SELECT Bordereau_Month, Agency, Writing_Company, Insured_Name, Insured_Zip, policyID, Policy_Num, Expiring_Policy_Num,
Policy_Eff_Date, Policy_Exp_Date, Trans_Date, Trans_Eff_Date,
Change_Type_Name, Coverage,
limits, deductible,
CASE
    WHEN status = 1 AND Change_Type_Name = 'New Policy' THEN Num_Vehicles
    WHEN status = 3 AND Change_Type_Name = 'New Policy' THEN 0*Num_Vehicles
    WHEN status = 3 AND Change_Type_Name <> 'New Policy' THEN -1*Num_Vehicles
    END AS Num_Vehicles,
Written_Premium, Payment_Plan,
State_Fee_or_Tax,
State_Fee_or_Tax_Description,
Agency_Fee, Carrier_Fees,
round(Written_Premium*0.0465, 2) AS Agency_Comm,
round(Written_Premium + State_Fee_or_Tax - round(Written_Premium*0.0465, 2), 2) AS Net_Amount,
'TX' AS Risk_State,
round(Written_Premium*0.016, 2) AS Premium_Tax
FROM half_completed_table
WHERE Trans_Date BETWEEN LastMonthStart AND LastMonthEnd
ORDER BY policy_Num, Policy_Eff_Date, Trans_Date, Trans_Eff_Date,
    CASE
        WHEN Change_Type_Name = 'New Policy' THEN '1'
        WHEN Change_Type_Name = 'Renewal' THEN '2'
        WHEN Change_Type_Name = 'Endorsement' THEN '3'
        WHEN Change_Type_Name = 'Cancellation' THEN '4'
        ELSE Change_Type_Name END,
    CASE
        WHEN coverage = 'BI' THEN '1'
        WHEN coverage = 'PD' THEN '2'
        WHEN coverage = 'COMP' THEN '3'
        WHEN coverage = 'COLL' THEN '4'
        WHEN coverage = 'PIP' THEN '5'
        WHEN coverage = 'UIMBI' THEN '6'
        WHEN coverage = 'UIMPD' THEN '7'
        ELSE coverage END