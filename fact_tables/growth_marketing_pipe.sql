

WITH base_table as (
   SELECT * ,
          CAST(insuredAddDate AS DATE) AS insuredAddDate
        , CAST(effectiveDate AS DATE) AS effectiveDateDate
        , CAST(fTouchDate AS DATE) as fTouchDate
        , CAST(lTouchDate AS DATE) as lTouchDate
        , CAST(applicationDate AS DATE) as applicationDate
    FROM silvervine_deduplicated.accounts
    )

, injection as (
     SELECT insuredID
            , policyID
            , 1 as signal
            , addDate
        FROM base_table
        WHERE addDate between '2021-11-30' AND '2022-01-29'
        AND status = 'RC1A'
    )

  , clean_table as (
    SELECT base_table.policyID
    , base_table.status
    , base_table.insuredID
    , policyNum
    , injection.addDate
    , base_table.insuredAddDate
    , base_table.fTouchDate
    , base_table.lTouchDate
    , base_table.applicationDate
    , number_of_touches
    , email
    , phone2
    , fname
    , lname
    , age
    , gender
    , maritalStatus
    , number_of_vehicles
    , number_of_drivers
    , num_points_charged
    , AVG_DHRF
    , ubiScoreTier
    , BI
    , PD
    , MEDPAY
    , PIP
    , RENT
    , COLL
    , OTC
    , UIMBI
    , UIMPD
    , LOAN
    , street
    , street2
    , city
    , state
    , zipcode
    , county
    , planName
    ,eDelivery
    , requiresSR22
    , OEM
    , AEB
    , RSA
    , months_since_last_aaf
    , accident_free_2
    , accident_free_5
    , naf_count
    , comp_1000
    , prior_insurance_category
    , premiumTotal
    , updated
    , signal

       FROM base_table
       LEFT JOIN injection
        ON injection.policyID = base_table.policyID
        AND injection.insuredID = base_table.insuredID
        WHERE injection.signal IS NULL
)
## add logic for quoted vs binded
, qb_logic as (
    SELECT policyID
         , insuredID
         , status
         , CASE
               WHEN status = "RC1A" THEN FALSE
               WHEN status = "RC1B" THEN TRUE
               WHEN status = "RC2" THEN TRUE
               WHEN status = "Not_Written" THEN TRUE
               WHEN status = "Active" THEN TRUE
               WHEN status = "Canceled" THEN TRUE
               WHEN status = "Expired" THEN TRUE
               WHEN status = "Binded" THEN TRUE
               ELSE FALSE END quoted
         , CASE
               WHEN status = "RC1A" THEN FALSE
               WHEN status = "RC1B" THEN FALSE
               WHEN status = "RC2" THEN FALSE
               WHEN status = "Not_Written" THEN FALSE
               WHEN status = "Active" THEN TRUE
               WHEN status = "Canceled" THEN TRUE
               WHEN status = "Expired" THEN TRUE
               WHEN status = "Binded" THEN TRUE
               ELSE FALSE END binded
         , CASE
               WHEN status = "RC1A" THEN 1
               WHEN status = "RC1B" THEN 2
               WHEN status = "RC2" THEN 3
               WHEN status = "Not_Written" THEN 4
               WHEN status = "Binded" THEN 5
               WHEN status = "Active" THEN 6
               WHEN status = "Canceled" THEN 7
               WHEN status = "Expired" THEN 8
               ELSE 0 END     status_order
         , policyNum
         , addDate
         , insuredAddDate
         , fTouchDate
         , lTouchDate
         , number_of_touches
         , email
         , phone2
         , fname
         , lname
         , gender
         , age
         , maritalStatus
         , number_of_vehicles
         , number_of_drivers
         , num_points_charged
         , AVG_DHRF
         , ubiScoreTier
         , BI
         , PD
         , MEDPAY
         , PIP
         , RENT
         , COLL
         , OTC
         , UIMBI
         , UIMPD
         , LOAN
         , street
         , street2
         , city
         , state
         , zipcode
         , county
         , planName
         , eDelivery
         , requiresSR22
         , OEM
         , AEB
         , RSA
         , months_since_last_aaf
         , accident_free_2
         , accident_free_5
         , naf_count
         , comp_1000
         , prior_insurance_category
         , premiumTotal
         , updated
    FROM clean_table
)
#because we know affect
, standardize_date as (

SELECT * FROM qb_logic)



SELECT * FROM qb_logic;
