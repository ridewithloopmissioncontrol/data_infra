# grab emails

WITH email_select as (
SELECT email
, InsuredID
FROM `roadrisk-244718`.silvervine.Insured
)

, policy_select AS(

    SELECT policyID
             , InsuredID
             , policyNum
             , paymentplanID
             , status
             #make a statusname table
             , CASE
                 WHEN status = 99 AND premiumTotal = 0
                     THEN 'RC1A'
                 WHEN status = 00 AND premiumTotal > 0
                     THEN 'RC1B'
                 WHEN status = 0
                     THEN 'RC2'
                 WHEN status = 1
                     THEN 'Active'
                WHEN status = 3
                     THEN 'Canceled'
                WHEN status = 5
                    THEN 'Expired'
                WHEN status = 6
                    THEN 'Not_Written'
                WHEN status = 4
                    THEN 'Declined'
                WHEN status = 2
                    THEN 'Bound'
                WHEN status = 98
                    THEN 'Submisson'
                ELSE NULL
                    END as statusname
             , premiumTotal
        FROM silvervine.Policy

    )

, PaymentPlan_Select AS (
        SELECT
               paymentPlanID
               , planName
        FROM `roadrisk-244718`.silvervine.PaymentPlan
    )
, coverage_select AS (
    SELECT policyID
    , policyType
    , coverage
    , CASE WHEN
        coverage = 'BI'
        THEN CONCAT(CAST(limit1 AS STRING) ," / ", CAST(limit2 AS STRING))
        ELSE CAST(limit1 AS STRING)
        END AS limits
    , premium
    , deductible

    FROM silvervine.Coverages
)

, vehicle_select AS (
    SELECT policyID
    , vehicleID
    , VIN
    FROM silvervine.Vehicle
)

, zip_select AS(
        SELECT zip.policyID
               ,zip.defaultGaragingAddressID
               ,zipcode
        FROM roadrisk-244718.silvervine.Auto zip
        JOIN
            (SELECT ad.id, ad.zipcode FROM silvervine.Address ad) ad_select
            ON zip.defaultGaragingAddressID = ad_select.id)



SELECT
       policy_select.policyID
        , policy_select.InsuredID
        , policy_select.policyNum
        , policy_select.paymentplanID
        , policy_select.status
        , policy_select.premiumTotal
        , policy_select.paymentPlanID
        , PaymentPlan_Select.planName
        ,coverage_select.policyType
        , coverage_select.coverage
        , coverage_select.limits
        , coverage_select.premium
        , coverage_select.deductible
        , vehicle_select.vehicleID
        , vehicle_select.VIN
FROM policy_select
LEFT JOIN email_select
    ON email_select.InsuredID = policy_select.InsuredID
LEFT JOIN PaymentPlan_Select
    ON PaymentPlan_Select.paymentPlanID = policy_select.paymentplanID
LEFT JOIN coverage_select
    ON coverage_select.policyID = policy_select.policyID
LEFT JOIN vehicle_select
    ON vehicle_select.policyID = policy_select.policyID;

SELECT * FROM silvervine.Vehicle;
