SELECT SUM(CASE WHEN ad_id IS NOT NULL THEN 1 ELSE 0 END)
, SUM(CASE WHEN ad_updated_time			IS NOT NULL THEN 1 ELSE 0 END) as ad_updated_time
, SUM(CASE WHEN index			IS NOT NULL THEN 1 ELSE 0 END) as index
, SUM(CASE WHEN _fivetran_synced		IS NOT NULL THEN 1 ELSE 0 END)	as _fivetran_synced
, SUM(CASE WHEN action_type			IS NOT NULL THEN 1 ELSE 0 END) as action_type
, SUM(CASE WHEN application			IS NOT NULL THEN 1 ELSE 0 END) as application
, SUM(CASE WHEN creative			IS NOT NULL THEN 1 ELSE 0 END) as create assignment
, SUM(CASE WHEN dataset			IS NOT NULL THEN 1 ELSE 0 END) as dataset
, SUM(CASE WHEN event			IS NOT NULL THEN 1 ELSE 0 END) as event
, SUM(CASE WHEN event_creator			IS NOT NULL THEN 1 ELSE 0 END) as event_creator
, SUM(CASE WHEN event_type			IS NOT NULL THEN 1 ELSE 0 END) as event_type
, SUM(CASE WHEN fb_pixel			IS NOT NULL THEN 1 ELSE 0 END) as fb_pixel
, SUM(CASE WHEN fb_pixel_event			IS NOT NULL THEN 1 ELSE 0 END) as fb_pixel_event
, SUM(CASE WHEN leadgen			IS NOT NULL THEN 1 ELSE 0 END) as leadgen
, SUM(CASE WHEN object			IS NOT NULL THEN 1 ELSE 0 END) as object
, SUM(CASE WHEN object_domain			IS NOT NULL THEN 1 ELSE 0 END) as object_domain
, SUM(CASE WHEN offer			IS NOT NULL THEN 1 ELSE 0 END) as offer
, SUM(CASE WHEN offer_creator			IS NOT NULL THEN 1 ELSE 0 END) as offer_creator
, SUM(CASE WHEN offsite_pixel			IS NOT NULL THEN 1 ELSE 0 END) as offsite_pixel
, SUM(CASE WHEN page			IS NOT NULL THEN 1 ELSE 0 END) as page
, SUM(CASE WHEN page_parent			IS NOT NULL THEN 1 ELSE 0 END) as page_parent
, SUM(CASE WHEN post			IS NOT NULL THEN 1 ELSE 0 END) as post
, SUM(CASE WHEN post_object			IS NOT NULL THEN 1 ELSE 0 END) as post_object
, SUM(CASE WHEN post_object_wall		IS NOT NULL THEN 1 ELSE 0 END)	as post_object_wall
, SUM(CASE WHEN post_wall			IS NOT NULL THEN 1 ELSE 0 END) as post_wall
, SUM(CASE WHEN question			IS NOT NULL THEN 1 ELSE 0 END) as question
, SUM(CASE WHEN question_creator		IS NOT NULL THEN 1 ELSE 0 END)	as question
, SUM(CASE WHEN response			IS NOT NULL THEN 1 ELSE 0 END) as response
, SUM(CASE WHEN subtype IS NOT NULL THEN 1 ELSE 0 END)
 FROM  `fivetranfbads.fb_fivetran.ad_conversion`

