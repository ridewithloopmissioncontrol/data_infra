WITH campaign as (
SELECT id
, name as campaign_name
, objective
, daily_budget
, status
, start_time
FROM `fivetranfbads.fb_fivetran.campaign_history`
)
, ad_set as (
SELECT id
, campaign_id
, billing_event
, budget_remaining
, daily_budget
, name as ad_set_name
,promoted_object_pixel_id
, targeting_age_max
, targeting_age_min
, targeting_flexible_spec 
FROM `fivetranfbads.fb_fivetran.ad_set_history` 
)
, ad as (
    SELECT
    id 
    ,ad_source_id
    ,ad_set_id
    ,campaign_id
    ,creative_id
    ,name as ad_name
    ,status

FROM `fivetranfbads.fb_fivetran.ad_history`
)

SELECT *
FROM campaign 
LEFT JOIN ad_set 
ON ad_set.campaign_id = campaign.id
LEFT JOIN ad 
ON ad.ad_set_id = ad_set.id;

SELECT * FROM `fivetranfbads.fb_fivetran.ad_set_history` ;
SELECT * FROM `fivetranfbads.fb_fivetran.campaign_history` LIMIT 10;
SELECT * FROM `fivetranfbads.fb_fivetran.ad_history` LIMIT 10;
SELECT * FROM `fivetranfbads.fb_fivetran.ad_tracking`;
SELECT * FROM `fivetranfbads.fb_fivetran.ad_conversion` 