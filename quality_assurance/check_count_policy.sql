SELECT
  DATE(addDate) AS quote_date,
  COUNT(DATE(addDate)) AS counts
FROM
  `datalakewarehouse.silvervine.Policy`
GROUP BY
  quote_date
ORDER BY
  quote_date DESC