WITH base_table as (
SELECT policyID
, DATE_ADD(extract(date from effectiveDate), INTERVAL -1 DAY) as Trans_Eff_Week
, DATE_ADD(extract(date from insuredAddDate), INTERVAL -1 DAY) as App_Date_Week
, status
 FROM `datalakewarehouse.d2a_datasets.silvervine_deduplicated_accounts`
)
# transform to push to Monday-Sunday cal
, base_table_week as (
SELECT policyID
, DATE_ADD(DATE_TRUNC(cast(App_Date_Week as DATE),week), INTERVAL 1 DAY) as AppDateWeek
, DATE_ADD(DATE_TRUNC(cast(Trans_Eff_Week as DATE),week), INTERVAL 1 DAY) as TransDateWeek
, status
FROM base_table
)

, count_table_bind as (
  SELECT
  COUNT(policyID) count_bind 
  , TransDateWeek
  FROM base_table_week
  WHERE status IN ('Cancelled', 'Active', 'Expired', 'Bound')
  GROUP BY TransDateWeek
)

, count_table_quote as (
  SELECT
  COUNT(policyID) count_app
  , AppDateWeek
  FROM base_table_week
  GROUP BY AppDateWeek
)

SELECT 
count_table_quote.count_app
, COALESCE(count_table_bind.count_bind, 0) as count_bind
, count_table_quote.AppDateWeek AS Week
FROM count_table_quote
LEFT JOIN count_table_bind
ON count_table_bind.TransDateWeek = count_table_quote.AppDateWeek
ORDER BY AppDateWeek DESC;